#!/bin/bash

# For loop over each apk
while read line 
do 
   # Retrieve the apk if not locally available
   #smbget -U student%hECp\>Nw8N\"5\$sua\( -r smb://macpro-amann.st.informatik.tu-darmstadt.de/GoogleAgeRatedApps/$line
   #move it to apks folder (EDIT)
   #mv $line /Users/ioannismittas/Desktop/BachelorPraktikum/pra/server/src/apks/

   # Run tool enjarify (Paste in your enjarify-master path and your apk path)
   cd /root/Downloads/enjarify-master
   python3 -O -m enjarify.main -f /root/Desktop/bp25/pra/apkExtract/apks/$line

   # Run extractor tool on generated output (Paste in your methodviewextractor path)
   cd /root/Downloads/methodviewextractor

   # Run methodviewextractor and save results for upload (Paste in your enjarify-master path, 
   # create a folder methods and paste in its path to save the extracted methods of each apk)
   java -jar methodviewextractor.jar /root/Downloads/enjarify-master/${line%%.apk}-enjarify.jar /root/Desktop/bp25/pra/apkExtract/methods/${line%%.apk}.txt

# Specify the path to the text file from which to read the apk names
done < /root/Desktop/bp25/pra/apkExtract/apks.txt

# Run this script with the command: sudo sh ./methodExtract.sh

