# appcheck
## Step 1: Getting started

[Clone](https://bitbucket.org/OPAL-Project/pra/src/master/) the git repository from Bitbucket and
download node.js [here](https://nodejs.org/en/download/) or (on mac) via terminal
`$ brew install node`.
Then [download](https://www.mongodb.com/download-center/community) the appropriate MongoDB version for your machine. The installation will follow in step 2.

## Step 2: Setting up the project

#### First open a terminal in the cloned repository and enter the following instructions:
#

``` bash
# Install the vue client globally
npm install -g vue-cli

# Go to the client folder
cd pra
cd client

# Install front end dependencies
npm install --save vuetify axios vuex vuex-router-sync vue-scrollto highlight.js vue-highlight.js

# Go to the server folder
cd ..
cd server

# Install and save back end dependencies
npm install --save nodemon express body-parser cors morgan mongoose-paginate mongoose graceful-fs recursive-readdir reiko-parser
```

#### Installing MongoDB (Mac):
#

``` bash
# Update Homebrew�s package database
brew update

# Install the MongoDB Binaries
brew install mongodb

# Create the data directory
mkdir -p /data/db
```

For Linux or Windows follow [this](https://docs.mongodb.com/manual/installation/#tutorial-installation) guide.

#### Inserting apks in the database:
#

Follow the instructions given as comments in the methodExtract.sh file and then the App.js file in the model directory.

#### Setting an index for the db:
#

``` bash
# Open the mongo shell in a terminal
mongo

# Choose your database
use <your db>

# Create the index
db.<your collection>.createIndex( { 'pkgInfo.application.label': "text" } )
```

#### More commands:
#

```bash
# Serve with hot reload at localhost:8080
npm run dev

# Build for production with minification
npm run build

# Build for production and view the bundle analyzer report
npm run build --report

# Run unit tests
npm run unit

# Run e2e tests
npm run e2e

# Run all tests
npm test

# Install dependencies
npm install

# Start MongoDB service
mongod
```

## Further steps

You now have a working Vue project setup on your machine. The following links guide through the setup and provide additional info.

-  [Express.js guide](http://expressjs.com/en/guide/routing.html)
- [MongoDB docs](https://docs.mongodb.com/?_ga=2.66666793.604089837.1543571359-957335953.1542975525)
- [Node.js docs](https://nodejs.org/en/docs/)
- [Npm docs](https://docs.npmjs.com/)
- [Vue.js guide](https://vuejs.org/v2/guide/)
- [Vuetify.js guide](https://vuetifyjs.com/en/getting-started/quick-start#existing-applications)


For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
