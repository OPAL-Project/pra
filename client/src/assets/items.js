// The list of items by which to sort/filter the applications
const items = [
  { title: 'Sort by name', icon: 'dashboard' },
  { title: 'COPPA conform', icon: 'C' },
  { title: 'All dangerous permissions', icon: 'security' },
  { title: 'Access contacts', icon: 'contacts' },
  { title: 'Call', icon: 'call' },
  { title: 'Access camera', icon: 'camera_alt' },
  { title: 'Access gps', icon: 'gps_fixed' },
  { title: 'Access microphone', icon: 'mic' },
  { title: 'Access phone numbers', icon: 'dialpad' },
  { title: 'Access voicemail', icon: 'voicemail' },
  { title: 'Use sip', icon: 'dialer_sip' },
  { title: 'Body sensors', icon: 'question_answer' },
  { title: 'Sms', icon: 'textsms' },
  { title: 'Receive wap push', icon: 'call_received' },
  { title: 'Access mms', icon: 'mms' },
  { title: 'Access storage', icon: 'storage' },
  { title: 'Access calendar', icon: 'calendar_today' },
  { title: 'Read cell broadcasts', icon: 'cast_connected' }
]

export default items
