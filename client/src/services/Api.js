// Import axios as api dependency
import axios from 'axios'

// Establishes a connection to the server for http requests
export default () => {
  return axios.create({
    baseURL: `http://localhost:8081/`
  })
}
