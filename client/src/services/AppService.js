// Imports the axios api
import Api from '@/services/Api'

export default {
  // Requesting the clients search from the server
  index (search) {
    return Api().get('browse', {
      params: {
        search: search
      }
    })
  },
  // Requesting an app with given appId from the server
  show (appId) {
    return Api().get(`browse/${appId}`)
  },
  // Requesting a given page of apps from the server
  showPage (page) {
    return Api().get('browse', {
      params: {
        page: page
      }
    })
  }
}
