// Imports of the site components
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Browse from '@/components/Browse'
import AppView from '@/components/AppView'

// Used for routing between components
Vue.use(Router)

// Export all component routes
export default new Router({
  // Removes the # from the url
  mode: 'history',
  // Routes of the site components
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/browse',
      name: 'browse',
      component: Browse
    },
    {
      path: '/browse/:appId',
      name: 'appview',
      component: AppView
    }
  ]
})
