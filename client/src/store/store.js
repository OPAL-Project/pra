// Imports for the store
import Vue from 'vue'
import Vuex from 'vuex'

// Use vuex to store values
Vue.use(Vuex)

// Attributes for the store
export default new Vuex.Store({
  strict: true,
  state: {},
  mutations: {},
  actions: {}
})
