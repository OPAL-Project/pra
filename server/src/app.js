// Import dependencies
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const app = express()
var mongoose = require('mongoose')
const AppsController = require('./controller/AppsController')

// Shows device log on console
app.use(morgan('combined'))
// Enable dependencies
app.use(bodyParser.json())
app.use(cors())

// Connect to the database
mongoose.connect('mongodb://localhost:27017/testdb')
var db = mongoose.connection
// Console log if it was succesfull or not
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', function (callback) {
  console.log('Connection Succeeded')
})

// Get-request for browse with search to database
app.get('/browse', AppsController.index)
// Get-request for appView to database
app.get('/browse/:appId', AppsController.show)
// Server listens to port 8081 on localhost
app.listen(process.env.PORT || 8081)
