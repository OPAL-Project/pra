// To load apks into to database uncomment the marked lines,
// start the server one time and then recomment them again
// If memory errors occur try to start the server with: node --max_old_space_size=4096 Apps.js
// from the terminal on path server/model
// Link to the apktool: https://github.com/JChord/reiko-parser

// Import of dependencies
var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')

// Defines a schema for the database
var appSchema = new mongoose.Schema({
  docs: { type: Array, required: false },
  total: { type: Number, required: false },
  limit: { type: Number, required: false },
  page: { type: Number, required: false },
  pages: { type: Number, required: false }
})

// Adds the paginate method to the app model
appSchema.plugin(mongoosePaginate)

// Creates and exports the the model
var apps = mongoose.model('apps', appSchema)
module.exports = apps

// Define database connection url and connection options
// UNCOMMENT START
// var recursive = require('recursive-readdir')
// const PkgReader = require('reiko-parser')
// var fs = require('graceful-fs')
// var path = require('path')

// mongoose.connect('mongodb://localhost:27017/db', {
//   keepAlive: 100000000,
//   connectTimeoutMS: 100000000,
//   useNewUrlParser: true
// })
// var conn = mongoose.connection
// UNCOMMENT END

// NOTE: dirPath is the path to the apk files and
// methodsPath is the path to the extracted apk methods
// UNCOMMENT START
// var dirPath = '/your/apk/path'
// var methodsPath = '/your/method/path'
// UNCOMMENT END

// Iterate through the methods file and save them in an array
// UNCOMMENT START
// var mthd = []
// var mnames = []
// var i = 0
// recursive(methodsPath, function (err, methods) {
//   if (err) {
//     console.error('could not list the directory', err)
//     process.exit(1)
//   }
//   methods.forEach(function (method) {
//     fs.readFile(method, 'utf8', function (err, content) {
//       var methodName = path.basename(method, '.txt')
//       mnames[i] = methodName
//       if (err) {
//         console.error(err)
//       }
//       mthd[i] = content
//       i++
//     })
//   })
// })
// UNCOMMENT END

// APK TOOL
// UNCOMMENT START
// recursive(dirPath, function (err, files) {
//   if (err) {
//     console.error('could not list the directory', err)
//     process.exit(1)
//   }
//   // For each file the apktool opens the apk, reads the AndroidManifest and inserts the manifest data in the database
//   files.forEach(function (file) {
//     const reader = new PkgReader(file, 'apk', { withIcon: true })
//     // Checks the time when connecting ...
//     var apkname = path.basename(file, '.apk')
//     console.log('%d : %d : %d', new Date().getHours(), new Date().getMinutes(), new Date().getSeconds())
//     reader.parse((err, pkgInfo) => {
//     // and check the time it takes for each apk to load in the database
//       console.log('%d : %d : %d', new Date().getHours(), new Date().getMinutes(), new Date().getSeconds())
//       if (err) {
//         console.error('!!ERROR!!', err)
//       } else {
//       // Insert apk info and method in apps collection
//         for (var j = 0; j <= mnames.length; j++) {
//           if (mnames[j] === apkname) {
//             conn.collection('apps').insertMany([{ pkgInfo, methods: mthd[j] }])// pkgInfo.icon is encoded to base64
//             break
//           }
//         }
//       }
//     })
//   })
// })
// UNCOMMENT END
