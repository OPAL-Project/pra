// Imports the app model
const App = require('../model/Apps')

// Exports the database query
module.exports = {
  // Query for browse
  async index (req, res) {
    try {
      let apps = null
      const search = req.query.search
      if (search) {
        // Query if search input is given
        apps = await App.find(
          { $text: { $search: search } },
          { score: { $meta: 'textScore' } }
        ).sort({ score: { $meta: 'textScore' } })
      } else {
        // Get current page from client and set the app limit per page
        var currPage = parseInt(req.query.page)
        const options = {
          page: parseInt(currPage) || 1,
          limit: 40
        }
        // Query if no search input is given on Browse
        apps = await App.paginate({}, options, {})
      }
      res.json({ apps })
    } catch (err) {
      res.status(500).send({
        error: 'error has occured'
      })
    }
  },
  // Query for appView by id
  async show (req, res) {
    try {
      const app = await App.findById(req.params.appId)
      res.json({ app })
    } catch (err) {
      res.status(500).send({
        error: 'error has occured'
      })
    }
  }
}
