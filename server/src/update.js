// Defines Current Date and Time if a connection occurs
var CurrentDateTime = new Date()
var currentDate = CurrentDateTime.getFullYear() + '-' + (CurrentDateTime.getMonth() + 1) + '-' + CurrentDateTime.getDate()
var currentTime = CurrentDateTime.getHours() + ':' + CurrentDateTime.getMinutes() + ':' + CurrentDateTime.getSeconds()
var LastUpdate = currentDate + ' ' + currentTime

// Exports the  component
export default LastUpdate
